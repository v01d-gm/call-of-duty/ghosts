from trainerbase.memory import Address
from trainerbase.gameobject import GameInt, GameFloat
from memory import iw6sp64_ship, player_coords_pointer


hp = GameInt(iw6sp64_ship.exe + 0x3C9177C)

player_coords_address = Address(player_coords_pointer, [0x0])
player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address.inherit(new_add=0x4))
player_z = GameFloat(player_coords_address.inherit(new_add=0x8))
