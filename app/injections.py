from trainerbase.codeinjection import AllocatingCodeInjection
from memory import iw6sp64_ship, player_coords_pointer


infinite_ammo = AllocatingCodeInjection(
    iw6sp64_ship.exe + 0x4796D4,
    """
        pop rax

        mov rbx, rcx
        mov rbx, [rsp + 0x30]
        add rsp, 0x20

        push rax
    """,
    original_code_length=16,
    is_long_x64_jump_needed=True,
)


update_player_coords_pointer = AllocatingCodeInjection(
    iw6sp64_ship.exe + 0x46E03C,
    f"""
        pop rax

        push rbx
        mov rbx, {player_coords_pointer}
        mov [rbx], r13
        pop rbx

        movss [r13 + 0x00], xmm1
        movss xmm0, [rsp + 0x7C]
        subss xmm0, [r13 + 0x04]

        push rax
    """,
    original_code_length=18,
    is_long_x64_jump_needed=True,
)
