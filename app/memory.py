from types import SimpleNamespace
from trainerbase.memory import POINTER_SIZE, pm


iw6sp64_ship = SimpleNamespace(exe=pm.base_address)
player_coords_pointer = pm.allocate(POINTER_SIZE)
